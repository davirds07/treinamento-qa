require_relative '../../../tdd/calculadora'
# language: pt
When(/^os argumentos '(.*?)' e '(.*?)'$/) do |primeiro_numero, segundo_numero|
  @primeiro_numero = primeiro_numero.to_i
  @segundo_numero = segundo_numero.to_i
end

When(/^o resultado será (\d+)$/) do |resultado|
  raise "Deu Ruim, resultado obtido foi #{resultado}" unless @resultado == resultado.to_i
end

When(/^(?:efetuarmos|efetuei) a (soma|divisão)$/) do |tipo_operacao|
  calculadora = Calculadora.new
  if tipo_operacao == 'soma'
    @resultado = calculadora.soma(@primeiro_numero, @segundo_numero)
  else
    begin
      @resultado = calculadora.divisao(@primeiro_numero, @segundo_numero)
    rescue
      @resultado = 'Mensagem de Erro'
    end
  end
end

When(/^veremos uma mensagem de erro$/) do
  raise 'Deu ruim, sem mensagem de erro' unless @resultado == 'Mensagem de Erro'
end